## Installation
The module depends of token_filter and twig_tweak modules.
After the installation you should enable token replacement for you text format
here /admin/config/content/formats.


## TokenLink example usage:
- Node tokens:

  `[tokenlink:node:123]`

  `[tokenlink:node:32:task]` - with key for template

  `[tokenlink:node:32#MyTitle,_blank,Myclass active]` - with custom link title, target and class

- User entity tokens:

  `[tokenlink:user:1]`

- Comment link token:

  `[tokenlink:comment:1]`

## Media tokens
If media module enabled these tokens become available:
- This token for Image type media files only, which shows image preview with thumbnail image style.
`[tokenlink:media:1:thumbnail]`
- Token for Image media which open image in modal popup is
  `[tokenlink:media:1:modal]`

- This token for Document type media files only, which shows generic image preview for media document.
  `[tokenlink:media:2:document]`

## Commerce product tokens
Under the develompment...












