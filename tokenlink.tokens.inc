<?php

/**
 * @file
 * Token callbacks for the tokenlink module.
 */

use Drupal\comment\Entity\Comment;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Implements hook_token_info().
 */
function tokenlink_token_info() {
  $type = [
    'name' => t('Token link'),
    'description' => t('Tokens link of entity.'),
  ];

  // Entity token link group.
  $tokens['node'] = [
    'name' => t("Entity link token"),
    'description' => t('The entity link tokens.'),
  ];

  return [
    'types' => ['tokenlink' => $type],
    'tokens' => ['tokenlink' => $tokens],
  ];
}

/**
 * Implements hook_tokens().
 */
function tokenlink_tokens($type, array $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if ($type == 'tokenlink') {
    foreach ($tokens as $name => $original) {
      $token_param = '';
      $token_name = $name;
      if (strpos($name, '#') !== FALSE) {
        [$token_name, $token_param] = explode('#', $name);
      }
      $p = explode(':', $token_name);
      // Get mandatory token parameters.
      $token_entity_type = $p[0];
      $token_entity_id = $p[1];
      $token_key = !empty($p[2]) ? $p[2] : '';

      switch ($token_entity_type) {
        case 'node':
          $token_entity = Node::load($token_entity_id);
          break;

        case 'user':
          $token_entity = User::load($token_entity_id);
          break;

        case 'comment':
          $token_entity = Comment::load($token_entity_id);
          break;

        case 'media':
          if (\Drupal::service('module_handler')->moduleExists('media')) {
            $token_entity = Media::load($token_entity_id);
          }
          break;
      }

      // If token entity was not found, so return nothing.
      $token_content = '';
      if ($token_entity) {
        // Get token content.
        $token_content = tokenlink_token_link($token_entity, $token_key, $token_param);
      }
      else {
        \Drupal::logger('tokenlink')
          ->warning('Token @token refers to a non-existent entity!',
            ['@token' => $original]);
      }

      // Assign token content.
      $replacements[$original] = $token_content;
      // Add cache dependency from token entity.
      $bubbleable_metadata->addCacheableDependency($token_entity);
    }
  }

  return $replacements;
}

/**
 * Return entity token content.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   REferenced token entity object.
 * @param string $token_key
 *   Token template key.
 * @param string $token_param
 *   Token parameters string.
 *
 * @return \Drupal\Core\GeneratedLink
 *   Token entity content.
 */
function tokenlink_token_link(EntityInterface $entity, string $token_key, string $token_param) {
  $entity_title = $entity->label();
  $target = '_blank';
  $class = '';
  // Parse token parameters.
  if (isset($token_param)) {
    $param = explode(',', $token_param);
    $param = array_map('trim', $param);

    $entity_title = !empty($param[0]) ? $param[0] : $entity_title;
    $target = !empty($param[1]) ? $param[1] : '';
    $class = !empty($param[2]) ? $param[2] : '';
  }

  $op = [
    'absolute' => TRUE,
    'attributes' => [
      'title' => $entity_title,
      'target' => $target,
      'class' => $class,
    ],
  ];
  // Remove elements with empty values.
  $op = array_filter_recursive($op);
  $url = tokenlink_get_entity_link($entity, $op);
  // Render token with custom template.
  if ($token_key) {
    $render = [
      '#theme' => 'tokenlink',
      '#entity' => $entity,
      '#key' => $token_key,
      '#param' => array_merge(['url' => $url->toString()], $op['attributes']),
    ];

    // Attach js library for modal popup.
    if ($token_key == 'modal') {
      $render['#attached']['library'][] = 'core/drupal.dialog.ajax';
    }

    return \Drupal::service('renderer')->render($render);
  }
  // Create link.
  $link_object = Link::fromTextAndUrl($entity_title, $url);
  return $link_object->toString();
}

/**
 * Returm $url object of given entity.
 */
function tokenlink_get_entity_link(EntityInterface $entity, $options) {
  $url = $entity->toUrl('canonical')->setAbsolute()->setOptions($options);
  if ($entity->getEntityTypeId() == 'comment') {
    $url->setOption('fragment', 'comment-' . $entity->id());
  }

  return $url;
}

/**
 * Recursively filter an array.
 */
function array_filter_recursive(array $array, callable $callback = NULL) {
  $array = is_callable($callback) ? array_filter($array, $callback) : array_filter($array);
  foreach ($array as &$value) {
    if (is_array($value)) {
      $value = call_user_func(__FUNCTION__, $value, $callback);
    }
  }

  return $array;
}
